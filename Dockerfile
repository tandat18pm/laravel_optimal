FROM phusion/baseimage:focal-1.2.0
COPY . /var/www
WORKDIR /var/www
RUN apt-get update \
    && apt-get install -yq --no-install-recommends \
        apt-utils \
        apt-transport-https \
        ca-certificates \
        curl \
        software-properties-common \
        nginx \
        python3 \ 
    && LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php -y\
    && apt-get remove --purge -y software-properties-common \
    && ./docker/cleanup.sh
RUN apt-get update \
    && apt-get install -yq --no-install-recommends \
        php8.0-fpm \
        php8.0-mbstring \
        php8.0-gd \
        php8.0-curl \
        php8.0-common \
        php8.0-zip \
        php8.0-mysql \
        php8.0-cli \
        php8.0-dom \
    && ./docker/cleanup.sh
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
COPY ./docker/nginx/conf.d/app.conf /etc/nginx/sites-enabled/default
COPY ./docker/php/www.conf /etc/php/8.0/fpm/pool.d/www.conf
COPY ./docker/php/php-fpm.conf /etc/php/8.0/fpm/php-fpm.conf
COPY --chown=www-data:www-data . /var/www
USER root
RUN ["chmod", "+x", "./docker/config_docker.sh"]
RUN ["chmod", "+x", "./docker/start_service.sh"]
RUN ./docker/config_docker.sh
RUN ./docker/start_service.sh
EXPOSE 80
