#!/bin/sh
#config PHP
sed -i "s/;date.timezone =.*/date.timezone = UTC/" /etc/php/8.0/fpm/php.ini && \
sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" /etc/php/8.0/fpm/php.ini && \
sed -i "s/short_open_tag = Off/short_open_tag = On/" /etc/php/8.0/fpm/php.ini && \
sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 40M/" /etc/php/8.0/fpm/php.ini && \
sed -i "s/post_max_size = 8M/post_max_size = 40M/" /etc/php/8.0/fpm/php.ini 

mkdir -p /run/php && chown www-data:www-data /run/php
#show log nginx in console
ln -sf /dev/stdout /var/log/nginx/access.log
ln -sf /dev/stderr /var/log/nginx/error.log
#install composer
composer install