#!/bin/sh
mkdir /etc/service/nginx
mkdir /etc/service/php
echo '#!/bin/sh \nexec /usr/sbin/nginx -g "daemon off;"' >> /etc/service/nginx/run
echo '#!/bin/sh \nexec /usr/sbin/php-fpm8.0' >> /etc/service/php/run
chmod +x /etc/service/nginx/run
chmod +x /etc/service/php/run